﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour {

    public BulletMove bulletPrefab;
    private float reloadTime = 0.5f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //do not run if the game is paused
        if (Time.timeScale == 0) {
            return;
        }

        //When the button is pushed, fire a bullet
        if (Input.GetButtonDown("Fire1")) {
            if (reloadTime <= 0) {
                BulletMove bullet = Instantiate(bulletPrefab);
                //the bullet starts at the player's position
                bullet.transform.position = transform.position;

                //create a ray towards the mouse location
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                bullet.direction = ray.direction;

                //reset the reload timer
                reloadTime = 0.5f;
            }
        }

        //decrease the reload timer
        reloadTime -= Time.deltaTime;
    }
}
