﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {
    //separate the speed and direction so we can tune the speed without changing code
    public float speed = 10.0f;
    public float lifeTime = 10.0f;
    public Vector3 direction;
    private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        rigidbody.velocity = speed * direction;

        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0) {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter(Collision collision) {
        //Destory the bullet
        Destroy(gameObject);
    }
}
