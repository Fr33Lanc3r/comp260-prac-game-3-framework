﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShellMenu : MonoBehaviour {

    public GameObject shellPannel;
    public GameObject optionsPannel;
    public Dropdown qualityDropdown;
    public Dropdown resolutionDropdown;
    public Toggle fullScreenToggle;
    public Slider volumeSlider;
    public Slider musicSlider;
    private bool paused = true;
    public AudioSource backgroundMusic;
 

	// Use this for initialization
	void Start () {
        //options pannel is initially hidden
        optionsPannel.SetActive(false);

        SetPaused(paused);

        //populate the list of video quality levels
        qualityDropdown.ClearOptions();
        List<string> names = new List<string>();
        for(int i = 0; i < QualitySettings.names.Length; i++) {
            names.Add(QualitySettings.names[i]);
        }
        qualityDropdown.AddOptions(names);

        //populate the list of available resolutions
        resolutionDropdown.ClearOptions();
        List<string> resolutions = new List<string>();
        for (int i = 0; i < Screen.resolutions.Length; i++) {
            resolutions.Add(Screen.resolutions[i].ToString());
        }
        resolutionDropdown.AddOptions(resolutions);

        //Restore the saved audio volume
        if (PlayerPrefs.HasKey("AudioVolume")) {
            AudioListener.volume = PlayerPrefs.GetFloat("AudioVolume");
        } else {
            //First time the game is run, use the default value
            AudioListener.volume = 1;
        }

        //Set Background Music to ignore AudioListener
        backgroundMusic.ignoreListenerVolume = true;
        //Restore the saved background volume
        if (PlayerPrefs.HasKey("BackgroundVolume")) {
            backgroundMusic.volume = PlayerPrefs.GetFloat("BackgroundVolume");
        } else {
            //First time the game is run, use the default value
            backgroundMusic.volume = 1;
        }
    }
	
	// Update is called once per frame
	void Update () {
		//pause if the player presses escape
        if (!paused && Input.GetKeyDown(KeyCode.Escape)) {
            SetPaused(true);
        }
	}

    private void SetPaused(bool p) {
        //make the shell panel (in)active when (un)paused
        paused = p;
        shellPannel.SetActive(paused);
        Time.timeScale = paused ? 0 : 1;
    }

    public void OnPressedPlay() {
        //resume the game
        SetPaused(false);
    }

    public void OnPressedQuit() {
        //quit the game
        Application.Quit();
    }

    public void OnPressedOptions() {
        //show the options pannel & hide the shell pannel
        shellPannel.SetActive(false);
        optionsPannel.SetActive(true);

        //select the current quality value
        qualityDropdown.value = QualitySettings.GetQualityLevel();

        //select the current resolution
        int currentResolution = 0;
        for (int i = 0; i <Screen.resolutions.Length; i++) {
            if (Screen.resolutions[i].width == Screen.width && Screen.resolutions[i].height == Screen.height) {
                currentResolution = i;
                break;
            }
        }
        resolutionDropdown.value = currentResolution;

        //set the fullscreen toggle
        fullScreenToggle.isOn = Screen.fullScreen;

        //set the volume slider
        volumeSlider.value = AudioListener.volume;

        //set the music slider
        musicSlider.value = backgroundMusic.volume;
    }

    public void OnPressedCancel() {
        //return to the shell menu
        shellPannel.SetActive(true);
        optionsPannel.SetActive(false);
    }

    public void OnPressedApply() {
        //apply the changes
        QualitySettings.SetQualityLevel(qualityDropdown.value);
        Resolution res = Screen.resolutions[resolutionDropdown.value];
        Screen.SetResolution(res.width, res.height, fullScreenToggle.isOn);
        AudioListener.volume = volumeSlider.value;
        PlayerPrefs.SetFloat("AudioVolume", AudioListener.volume);
        backgroundMusic.volume = musicSlider.value;
        PlayerPrefs.SetFloat("BackgroundVolume", backgroundMusic.volume);
        //return to the shell menu
        shellPannel.SetActive(true);
        optionsPannel.SetActive(false);
    }
}
